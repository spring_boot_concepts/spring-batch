package com.spring_batch_mongo.spring_batch.config;


import com.spring_batch_mongo.spring_batch.model.Person;
import com.spring_batch_mongo.spring_batch.util.MailUtil;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.data.MongoItemReader;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.oxm.xstream.XStreamMarshaller;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ApplicationConfig {
    @Autowired
    JobBuilderFactory jobBuilderFactory;
    @Autowired
    StepBuilderFactory stepBuilderFactory;
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    MailUtil mailUtil;

    @Bean
    public MongoItemReader<Person> reader(){
        MongoItemReader<Person> mongoItemReader=new MongoItemReader<Person>();
        mongoItemReader.setTemplate(mongoTemplate);
        mongoItemReader.setQuery("{}");
        mongoItemReader.setTargetType(Person.class);
        mongoItemReader.setSort(new HashMap<String, Sort.Direction>(){{
            put("custId",Sort.Direction.DESC);
        }});
        return mongoItemReader;
    }

    @Bean
    public StaxEventItemWriter<Person> writter( ) {
        StaxEventItemWriter<Person> writer=new StaxEventItemWriter<Person>();
        writer.setRootTagName("Persons");
        writer.setResource(new FileSystemResource("xml/bank.xml"));
        writer.setMarshaller(marshaller());
        return writer;
    }
    public XStreamMarshaller marshaller() {
        XStreamMarshaller marshall=new XStreamMarshaller();
        Map<String,Class> map=new HashMap<>();
        map.put("Person",Person.class);
        marshall.setAliases(map);
        return marshall;
    }

    public ItemProcessor<Person,Person> process(){
        ItemProcessor<Person,Person> itemProcessor=new ItemProcessor<Person, Person>() {
            @Override
            public Person process(Person person) throws Exception {
                if(person.getStatus().equalsIgnoreCase("pending")) {
                    String msg = mailUtil.sendEmail(person.getEmail(),buildMessage(person));
                    System.out.println(msg);
                    return person;
                }
                return null;
            }
            private String buildMessage(Person person){
                String mailBody="Dear "+person.getName()+","+"\n"+"Your statement of credit card ending with "+person.hashCode()+
                        "has been generated"+"\n"+"Due amount to be paid for this month "+person.getOutStandingAmount()
                        +"\n"+"Kindly pay the amount as soon as possible to avoid any kind of penalty."+
                        "\n"+"Thank you for Banking with us";
                return mailBody;
            }
        };
        return itemProcessor;
    }

    @Bean
    public Step step1(){
        return stepBuilderFactory.get("step1").<Person,Person>chunk(10).reader(reader()).processor(process()).writer(writter()).build();
    }
    @Bean
    public Job runJob(){
        return jobBuilderFactory.get("report generation").flow(step1()).end().build();
    }
}
