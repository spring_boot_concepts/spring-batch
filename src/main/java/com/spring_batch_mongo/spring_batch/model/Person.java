package com.spring_batch_mongo.spring_batch.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@Document(collection = "Bank Users")
public class Person {

    @Id
    private int _id;
    private String name;
    private String email;
    private String contactNo;
    private String dob;
    private double outStandingAmount;
    private String status;
    private Date dueDate;
}
