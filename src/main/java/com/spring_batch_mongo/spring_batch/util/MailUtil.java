package com.spring_batch_mongo.spring_batch.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailUtil {

    @Autowired
    private JavaMailSender mailSender;

    public String sendEmail(String to,String textBody){
        String msg="";
        SimpleMailMessage message=new SimpleMailMessage();
        try{
            message.setTo(to);
            message.setText(textBody);
            message.setSubject("Payment Due Alert");
            message.setFrom("salamon1512@gmail.com");
            mailSender.send(message);
            msg="mail was sent successfully";
        }
        catch (Exception e){
            msg=e.getMessage();
        }
        return msg;
    }
}
